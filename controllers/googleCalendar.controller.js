const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const zoomMeetings = db.zoomMeetings;

var var_arr = [];


exports.update = (req, res) => {
    if (req.body.googleCalendarCode == null || req.body.userId == null) {
        return res.json({ status: 'error', message: 'googleCalendarCode or userId in request not found' });
    }

    var eventsArray = [];
    User.update({
        googleCalendarCode: req.body.googleCalendarCode,
    }, {
        where: { id: req.body.userId }
    })
        .then(num => {

            const tkn = req.body.googleCalendarCode;
            const fs = require('fs');
            const { google } = require('googleapis');

            const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
            // The file token.json stores the user's access and refresh tokens, and is
            // created automatically when the authorization flow completes for the first
            // time.
            const TOKEN_PATH = 'tokens/' + req.body.userId + 'token.json';

            // Load client secrets from a local file.
            fs.readFile('credentials.json', (err, content) => {
                if (err) return console.log('Error loading client secret file:', err);
                // Authorize a client with credentials, then call the Google Calendar API.
                authorize(JSON.parse(content), listEvents);
            });

            /**
             * Create an OAuth2 client with the given credentials, and then execute the
             * given callback function.
             * @param {Object} credentials The authorization client credentials.
             * @param {function} callback The callback to call with the authorized client.
             */
            function authorize(credentials, callback) {
                const { client_secret, client_id, redirect_uris } = credentials.installed;
                const oAuth2Client = new google.auth.OAuth2(
                    client_id, client_secret, redirect_uris[0]);

                // Check if we have previously stored a token.
                fs.readFile(TOKEN_PATH, (err, token) => {
                    if (err) return getAccessToken(oAuth2Client, callback);
                    oAuth2Client.setCredentials(JSON.parse(token));
                    callback(oAuth2Client);
                });
            }

            /**
             * Get and store new token after prompting for user authorization, and then
             * execute the given callback with the authorized OAuth2 client.
             * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
             * @param {getEventsCallback} callback The callback for the authorized client.
             */
            function getAccessToken(oAuth2Client, callback) {
                oAuth2Client.getToken(tkn, (err, token) => {
                    if (err) return console.error('Error retrieving access token', err);
                    oAuth2Client.setCredentials(token);
                    // Store the token to disk for later program executions
                    fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                        if (err) return console.error(err);
                        console.log('Token stored to', TOKEN_PATH);
                    });
                    callback(oAuth2Client);
                });
            }

            /**
             * Lists the next events on the user's primary calendar.
             * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
             */
            function listEvents(auth) {
                async function fun() {
                    const calendar = await google.calendar({ version: 'v3', auth });
                    calendar.events.list({
                        calendarId: 'primary',
                        timeMin: (new Date()).toISOString(),
                        maxResults: 30,
                        singleEvents: true,
                        orderBy: 'startTime',
                    }, (err, res) => {
                        if (err) return console.log('The API returned an error: ' + err);
                        const events = res.data.items;
                        if (events.length) {
                            // console.log('Your upcoming events:', events);
                            eventsArray = events;
                            events.forEach((event, i) => {
                                if (event.start.timeZone === null) {
                                    console.log(`Event with summary: ${event.summary} Insertion failed. Because this Google calendar event is not complete, it hasn't had a timezone`)
                                } else {
                                    var url = event.description;
                                    var regEx = /https:[/][/]us0[\d]web.zoom.us[/][a-z][/][\d]+[?]/

                                    if (url.match(regEx)) {
                                        var results = url.match(regEx)
                                        var httpLink = results.toLocaleString()
                                        var meetingIds = /\d{10,}/
                                        if (httpLink.match(meetingIds)) {
                                            var Id = httpLink.match(meetingIds)
                                            var meetingId = Id.toLocaleString()

                                            zoomMeetings.create({
                                                summary: event.summary,
                                                zoomMeetingId: meetingId,
                                                startDateTime: event.start.dateTime,
                                                timeZone: event.start.timeZone,
                                                userId: req.body.userId
                                            }).then((user) => {
                                                console.log(`meeting id: ${meetingId} with summary ${event.summary} inserted for userId ${req.body.userId}`)
                                            }).catch(console.error);

                                        } else {
                                            console.log('Meeting ID is Wrong')
                                        }
                                    }
                                    else {
                                        console.log('No Meeting ID')
                                    }
                                }
                            });
                        } else {
                            console.log('No upcoming events found.');
                        }
                    });
                }
                fun()
            }
            res.send({
                googleCalendarCode: `${req.body.googleCalendarCode} `
            });
        })
        .catch(err => {
            res.status(500).send({
                err: "Error inserting googleCalendarCode"
            });
        });
};