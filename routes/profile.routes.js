const { authJwt } = require("../middleware");
const controller = require("../controllers/profile.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get('/profileData', [authJwt.verifyToken], controller.profile);

    app.get('/profile',  controller.index);
    app.post('/profile',  controller.index);

};
