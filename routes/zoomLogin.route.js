var jwt = require("jsonwebtoken");
const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const request = require('request')
require('dotenv/config')

module.exports = app => {

    app.get('/auth/zoom', (req, res) => {

        // Check if the code parameter is in the url 
        // if an authorization code is available, the user has most likely been redirected from Zoom OAuth
        // if not, the user needs to be redirected to Zoom OAuth to authorize

        if (req.query.code) {

            // Request an access token using the auth code

            let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.Zoom_redirectURL;

            request.post(url, (error, response, body) => {

                // Parse response to JSON
                body = JSON.parse(body);

                // Logs of access and refresh tokens
                console.log(`access_token: ${body.access_token}`);
                console.log(`refresh_token: ${body.refresh_token}`);

                if (body.access_token) {

                    // We can now use the access token to authenticate API calls

                    // Send a request to get your user information using the /me context

                    request.get('https://api.zoom.us/v2/users/me', (error, response, body) => {
                        if (error) {
                            console.log('API Response Error: ', error)
                        } else {
                            body = JSON.parse(body);
                            // Display response in console
                            console.log('API call ', body);
                            console.log('email ', body.email);
                            console.log('zoom id is ', body.id)
                            var email = body.email
                            User.findAll({ where: { email: email } })
                                .then(user => {
                                    console.log("data is: ", user);
                                    console.log("email in data is: ", body.email);
                                    if (user.length === 0) {
                                        // Save User to Database
                                        User.create({
                                            zoomId: body.id,
                                            firstName: body.first_name,
                                            lastName: body.last_name,
                                            email: body.email,
                                            roles: ["user"]
                                        }).then((user) => {
                                            var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                                                expiresIn: 86400 // 24 hours
                                            });

                                            console.log('token and user data sent: ', { "userEmail": body.email, jwtToken });
                                            res.status(200).send({
                                                zoomId: body.id,
                                                id: user.id,
                                                firstName: body.first_name,
                                                lastName: body.last_name,
                                                email: body.email,
                                                accessToken: jwtToken,
                                                googleCalendarCode: user.googleCalendarCode
                                            });
                                        }).catch(console.error)
                                    } else {
                                        var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                                            expiresIn: 86400 // 24 hours
                                        });
                                        console.log('token and user data sent: ', { "userId": body.id, "userEmail": body.email, jwtToken });

                                        User.update({
                                            zoomId: body.id,
                                        },
                                            {
                                                where: {
                                                    email: body.email
                                                }
                                            }).then((user) => {

                                                res.status(200).send({
                                                    zoomId: body.id,
                                                    id: user.id,
                                                    firstName: body.first_name,
                                                    lastName: body.last_name,
                                                    email: body.email,
                                                    accessToken: jwtToken,
                                                    googleCalendarCode: user.googleCalendarCode
                                                });

                                            }).catch(console.error)


                                    }

                                })
                                .catch(err => {
                                    res.status(500).send({
                                        message: "Error while zoom login"
                                    });
                                });
                        }
                    }).auth(null, null, true, body.access_token);

                } else {
                    console.log("zoom error");
                    res.status(500).send({
                        message: "Error while zoom login"
                    });
                }

            }).auth(process.env.Zoom_clientID, process.env.Zoom_clientSecret);

            return;

        }

        // If no authorization code is available, redirect to Zoom OAuth to authorize
        res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.Zoom_clientID + '&redirect_uri=' + process.env.Zoom_redirectURL)
    })

}