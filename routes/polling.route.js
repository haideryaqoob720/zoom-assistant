const db = require("../models");
const { Op } = require("sequelize");
var nodemailer = require('nodemailer');
const config = require("../config/auth.config");
const User = db.user;
const zoomMeetings = db.zoomMeetings;
const emailSentRecord = db.emailSentRecord;
const request = require('request');
require('dotenv/config')

const DELAY = config.GetLatestDataAfter;

var counter = 0;

setTimeout(async function run() {

  // Just fetching users busy in call and have had google calender authenticated on our server

  var requiredUsers = await User.findAll({
    where: {
      [Op.and]: [
        {
          googleCalendarCode: {      // User is Authenticated by google Calendar
            [Op.not]: null
          }
        },
        {
          [Op.or]: [                 // User is in call
            { UserMeetingStatus: 'In_Meeting' },
            { UserMeetingStatus: 'Presenting' },
            { UserMeetingStatus: 'On_Phone_Call' }
          ]
        }]
    }
  });
  if (requiredUsers.length === 0) {
    console.log("users not exist");
    // return res.json({ status: 'ok' });
  } else {
    // console.log("required user's are: ", requiredUsers);

    // Deleting all the data of meetings table

    zoomMeetings.destroy({
      where: {},
      truncate: true
    })

    requiredUsers.forEach((user, i) => {
      console.log("Email of required user: ", user.email);

      // fetching latest data of users from google calender

      const options = {
        method: 'POST',
        url: `${config.server}/api/user/googleCalendarAuth`,
        headers: { 'content-type': 'application/json' },
        body: {
          googleCalendarCode: user.googleCalendarCode,
          userId: user.id

        },
        json: true
      };

      (async () => {
        try {
          const response = await request(options, function (error, response, body) {
            if (error) throw new Error(error);
            if (body.googleCalendarCode != null) {
              console.log("Meeting's Data updated successfully");

              // fetching latest meetings of user
              setTimeout(function () {

                zoomMeetings.findAll({
                  where: {
                    userId: {
                      [Op.eq]: user.id
                    }
                  }
                }).then((latestMeetings) => {

                  // Calculating time difference of each latest meeting corresponding to each user

                  latestMeetings.forEach((meet, i) => {
                    console.log(`Zoom Meeting time of userId: ${meet.userId} is ${meet.startDateTime} and timezone is: ${meet.timeZone} summary is: ${meet.summary}`)

                    // Finding time difference of meetings

                    var dateAndTime = new Date().toLocaleString("en-US", { timeZone: meet.timeZone })
                    console.log('Current Time & Date of that timeZone', dateAndTime);

                    var meetingTime = new Date(meet.startDateTime).toUTCString()
                    console.log('UTC meetingTime', meetingTime);

                    var d1 = new Date(dateAndTime);
                    var d2 = new Date(meetingTime);

                    var diffInMS = d2 - d1;
                    console.log('Remaining Time in mili seconds: ', diffInMS);

                    var diffInMnt = Math.floor((diffInMS / 1000 / 60) << 0);
                    var diffInSec = Math.floor((diffInMS / 1000) % 60);
                    var minuteAndSecond = diffInMnt + ':' + diffInSec;
                    console.log('Remaining Time in minutes: ', minuteAndSecond);

                    if (diffInMS < 1000) {
                      console.log('\x1b[33m%s\x1b[0m', '*******Result******* Meeting is ended', minuteAndSecond, 'mins ago');
                    } else {
                      if (diffInMS > config.emailSendingTimeToRegistrantsBeforeMeeting) {
                        console.log('\x1b[33m%s\x1b[0m', '*******Result******* time is more than 5 mnt', minuteAndSecond);
                      } else if (diffInMS <= config.emailSendingTimeToRegistrantsBeforeMeeting) {
                        console.log('\x1b[33m%s\x1b[0m', '*******Result******* time is less than 5 mnt', minuteAndSecond);

                        // fetching registrants of this meeting from zoom server

                        const regOptions = {
                          method: 'GET',
                          // url: `https://api.zoom.us/v2/metrics/meetings/${req.query.meetingId}/participants`,
                          url: `https://api.zoom.us/v2/meetings/${meet.zoomMeetingId}/registrants`,

                          headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
                          json: true
                        };

                        (async () => {
                          try {
                            const regResponse = await request(regOptions, function (error, response, body) {
                              if (error) throw new Error(error);

                              if (body.registrants) {

                                // Sending Emails to all registrants
                                setTimeout(async function () {

                                  body.registrants.forEach((regUser, i) => {
                                    console.log(`email of registrant is: ${regUser.email}`)

                                    emailSentRecord.findOne({
                                      where: {
                                        [Op.and]: [
                                          { userId: meet.userId },
                                          { meetingId: meet.zoomMeetingId }
                                        ]
                                      }
                                    }).then((record) => {
                                      if (record === null) {

                                        // Fetching Users First name and last name to send in email to all other registrants

                                        User.findOne({
                                          where: { id: meet.userId }
                                        }).then((userName) => {
                                          if (userName != null) {


                                            var transporter = nodemailer.createTransport({
                                              service: 'gmail',
                                              auth: {
                                                user: process.env.Send_Email_From,
                                                pass: process.env.Send_Email_Pwd
                                              }
                                            });

                                            var mailOptions = {
                                              from: process.env.Send_Email_From,
                                              to: regUser.email,
                                              subject: 'Zoom Assistant Notification',
                                              text: `Hi, ${userName.dataValues.firstName} ${userName.dataValues.lastName} is busy in meeting. So, User can be late`
                                            };

                                            transporter.sendMail(mailOptions, function (error, info) {
                                              if (error) {
                                                console.log(error);
                                              } else {
                                                console.log('Email sent: ' + info.response);

                                                // Send emails to registrants of meeting only once

                                                emailSentRecord.create({
                                                  userId: meet.userId,
                                                  meetingId: meet.zoomMeetingId,
                                                  receiverEmail: regUser.email
                                                }).then((record) => {
                                                  console.log(` Email Sent record created: ${record.userId} and meeting ID: ${record.meetingId}`)
                                                }).catch(console.error);
                                              }
                                            });

                                          } else {
                                            console.log("user not exists for sending email")
                                          }
                                        }).catch(console.error);
                                      } else {
                                        console.log("Emails already sent!")
                                      }
                                    }).catch(console.error);
                                  });
                                }, 2000);

                              } else {
                                console.log(`Meeting ID: ${meet.zoomMeetingId} has no registrants or meeting invalid.`);
                              }
                            });
                          } catch (error) {
                            console.log(error);
                          }
                        })();


                      } else {
                        console.log('Time format is wrong');
                      }
                    }
                  });

                }).catch(console.error)

              }, 2000);

            } else {
              console.log("failed to update meetings data");
            }
          });
        } catch (error) {
          console.log(error);
        }
      })();

    });
  }
  console.log("Counter latest value is: ", ++counter);

  setTimeout(run, DELAY)
}, DELAY)



