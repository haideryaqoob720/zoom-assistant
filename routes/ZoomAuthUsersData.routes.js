const db = require("../models");
const User = db.user;
// const rp = require('request-promise');
const request = require('request')
const bodyParser = require('body-parser');
require('dotenv/config')

module.exports = app => {

    app.get('/create/meeting', (req, res) => {

        const options = {
            method: 'POST',
            url: `https://api.zoom.us/v2/users/${req.query.zoomUserId}/meetings`,
            headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
            body: {
                topic: 'Demo Meeting 2',
                type: 2,
                start_time: '2021-04-05 12:00:00',
                password: 'Hello123',
                agenda: 'This is the meeting description of demo meeting 2',
                settings: {
                    host_video: false,
                    participant_video: false,
                    join_before_host: false,
                    mute_upon_entry: true,
                    use_pmi: false,
                    approval_type: 0
                }
            },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(body);
        });


    })

    // adding registerant

    // app.get('/create/reg', (req, res) => {

    //     const options = {
    //         method: 'POST',
    //         url: `https://api.zoom.us/v2/meetings/${req.query.meetingId}/registrants`,
    //         headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
    //         body: {
    //             email: 'Demo Meeting 2',
    //             first_name: 2,
    //             last_name: '2021-04-05 12:00:00',
    //             address: 'Hello123',
    //             city: 'This is the meeting description of demo meeting 2',
    //             country: "xyz",
    //             zip: "1234",
    //             state: "US",
    //             phone: "456789",
    //             industry: "tech",
    //             org: "abc",
    //             job_title: "team lead",
    //             purchasing_time_frame: "3 months",
    //             role_in_purchase_process: "Influencer",
    //             no_of_employees: 
    //             settings: {
    //                 host_video: false,
    //                 participant_video: false,
    //                 join_before_host: false,
    //                 mute_upon_entry: true,
    //                 use_pmi: false,
    //                 approval_type: 0
    //             }
    //         },
    //         json: true
    //     };
    //     request(options, function (error, response, body) {
    //         if (error) throw new Error(error);
    //         console.log(body);
    //     });


    // })

    app.get('/fetch/meetingDetails', (req, res) => {

        const options = {
            method: 'GET',
            url: `https://api.zoom.us/v2/meetings//${req.query.zoomMeetingId}`,
            headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(body);
        });


    })

    app.get('/fetch/meeting', (req, res) => {

        const options = {
            method: 'GET',
            url: `https://api.zoom.us/v2/users/${req.query.zoomUserId}/meetings`,
            headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(body);
        });


    })

    app.get('/fetch/meetingParticipants', (req, res) => {

        const options = {
            method: 'GET',
            // url: `https://api.zoom.us/v2/metrics/meetings/${req.query.meetingId}/participants`,
            url: `https://api.zoom.us/v2/meetings/${req.query.meetingId}/registrants`,

            headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            if(body.registrants){
                console.log(body.registrants);
            }else {
                console.log(body);
            }
            
        });


    })

    app.get('/fetch/userInfo', (req, res) => {

        const options = {
            method: 'GET',
            url: `https://api.zoom.us/v2/users/${req.query.userId}`,
            headers: { 'content-type': 'application/json', authorization: `Bearer ${process.env.Zoom_JWT_Token}` },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(body);
        });


    })

    // Set up a webhook listener for your Webhook Event - in this case we are listening to user call status event.
    app.post('/userStatusWebHook', bodyParser.raw({ type: 'application/json' }), async (req, res) => {
        let event;

        try {
            event = JSON.parse(req.body);
        } catch (err) {
            res.status(400).send(`Webhook Error: ${err.message}`);
        }
        // Check to see if you received the event or not.
        // console.log("webhook event: ", req.body);
        if (req.headers.authorization === process.env.Zoom_Verification_Token) {
            res.status(200);

            res.send();
            var userEmail = req.body.payload.object.email;
            var userZoomStatus = req.body.payload.object.presence_status;
            console.log('\x1b[33m%s\x1b[0m', "Webhook! " + " User with email : " + userEmail + " zoom status is: " + userZoomStatus);

            //ensure that you have a user with this email
            var email = await User.findOne({ where: { email: userEmail } });
            if (email == null) {
                console.log("user not exist");
                // return res.json({ status: 'ok' });
            } else {
                await User.update({
                    UserMeetingStatus: req.body.payload.object.presence_status
                },
                    {
                        where: {
                            email: userEmail
                        }
                    });
            }


        } else {

            res.status(403).end('Access forbidden');
            console.log("Invalid Post Request.")
        }
    });

}