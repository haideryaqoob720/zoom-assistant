require('dotenv/config')

module.exports = {
    secret: process.env.Zoom_clientSecret,
    GetLatestDataAfter: 60000,    // Right now after 1 minute server will repeat the complete  and will get the latest data for each user and for each meeting
    server: "http://localhost:8080",
    emailSendingTimeToRegistrantsBeforeMeeting: 300000
  };