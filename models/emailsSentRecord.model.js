module.exports = (sequelize, Sequelize) => {
    const EmailSentRecord = sequelize.define("emailSentRecord", {
        userId: {
            type: Sequelize.STRING
        },
        meetingId: {
            type: Sequelize.STRING
        },
        receiverEmail: {
            type: Sequelize.STRING
        },
    });

    return EmailSentRecord;
};