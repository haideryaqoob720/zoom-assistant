module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    email: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    UserMeetingStatus: {
      type: Sequelize.STRING
    },
    firstName: {
      type: Sequelize.STRING
    },
    lastName: {
      type: Sequelize.STRING
    },
    zoomId: {
      type: Sequelize.STRING
    },
    imageName: {
      type: Sequelize.STRING
    },
    googleCalendarCode: {
      type: Sequelize.STRING
    }
  });

  return User;
};