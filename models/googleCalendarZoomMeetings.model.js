module.exports = (sequelize, Sequelize) => {
    const ZoomMeetings = sequelize.define("zoomMeetings", {
        summary: {
            type: Sequelize.STRING
        },
        zoomMeetingId: {
            type: Sequelize.STRING
        },
        startDateTime: {
            type: Sequelize.STRING
        },
        timeZone: {
            type: Sequelize.STRING
        }
    });

    return ZoomMeetings;
};